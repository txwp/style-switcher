<?php

class TXSwitcher{

	public $doc_url;
	public $cart_url;
	public $preset_path = '';
	public $bg_image_path = '';
	public $pattern_path = '';
	public $theme_path;

	public $cms;

	public function loadAsset(){
		if ( $this->cms === 'wordpress'){
			add_action( 'wp_enqueue_scripts', array($this, 'wp_style_switcher_enqueue') );
		}
	}

	public function loadHtml(){
		if( $this->cms === 'wordpress'){
			add_action( 'wp_footer', array($this, 'html') );
		}
	}

	public function wp_style_switcher_enqueue() {
	    //wp_enqueue_style('switcher-bs-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
	    //wp_enqueue_style('switcher-fa-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	    wp_enqueue_style( 'switcher-uikit-css', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.12/css/uikit.min.css');
        wp_enqueue_style( 'switcher-css', get_template_directory_uri() . '/switcher/dist/styles/master.css', array(), null );
	    wp_enqueue_style('tx-gf-body','/'); // enqueue for body font family
	    wp_enqueue_style('tx-gf-title','/'); // enqueue for title font family
	   // Js
	    wp_enqueue_script( 'switcher-uikit-js', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.12/js/uikit.min.js', array( 'jquery' ), null, true );
	    wp_enqueue_script( 'switcher-color-js', get_template_directory_uri() . '/switcher/js/jscolor.min.js', array( 'jquery' ), null, true );
	    wp_enqueue_script( 'switcher-main-js', get_template_directory_uri() . '/switcher/js/main.js', array( 'jquery' ), null, true );
	}

	public function html(){
	?>
	<div class="switcher-section">

    <div class="toggle-switcher">
        <a href="javascript:void(0)" class="switcher-spin">
            <i class="fa fa-cog fa-spin"></i>
        </a>
        <!-- <a href="#" class="switcher-documentation" target="_blank">
            <i class="fa fa-book"></i>
        </a> -->
        <a href="https://themeforest.net/checkout/25237924/billing_details" class="switcher-cart" target="_blank">
            <i class="fa fa-shopping-cart"></i>
        </a>
    </div>

    

    <div class="switcher-section-wrapper">

        <ul class="uk-child-width-expand switcher-tab" uk-tab>
            <li class="uk-active"><a href="#style-tab">Style</a></li>
            <li><a href="#customizer-tab">Customize</a></li>
            <li><a href="#typo-tab">Typography</a></li>
        </ul>

        <ul class="uk-switcher">

            <li class="style-tab uk-padding-small" id="style-tab">

                <div>
                    <label class="uk-form-label">Preset Layouts</label>
                </div>

                <div uk-grid>
                    <div class="uk-width-1-2@m">
                        <div class="uk-card uk-card-default">
                            <a href="/home/?home=true" ><img src="<?php echo get_template_directory_uri().'/dist/images/demo/presets/thumb1.jpg'; ?>" alt=""></a>
                        </div>
                    </div>

                    <div class="uk-width-1-2@m">
                        <div class="uk-card uk-card-default">
                            <a href="/home-2/?home=true" ><img src="<?php echo get_template_directory_uri().'/dist/images/demo/presets/thumb2.jpg'; ?>" alt=""></a>
                        </div>
                        
                    </div>

                   
                </div>

                <div class="layout uk-margin">

                    <label class="uk-form-label">Select Layout</label>

                    <select class="uk-select">
                        <option value="full-width">Full Width</option>
                        <option value="boxed">Boxed</option>
                    </select>



                    <div class="boxed-backgrond uk-margin">

                        <label class="uk-form-label">Background Image</label>
                        <ul class="boxed-bg-image">

                        <?php $a = 1 ;foreach( str_replace('/home/valley/web/valley.demo.themexpert.com/public_html/', 'http://valley.demo.themexpert.com/' , glob($this->bg_image_path . '/*.jpg'))  as $img ):?>
                        	
                        	<li data-bg-img="bg/<?php echo $a++; ?>.jpg">
                        		<img src="<?php echo $img; ?>">
                        	</li>
                        	
                        <?php endforeach;?>
                            
                        </ul>

                        <label class="uk-form-label">Background Pattern</label>
                        <ul class="boxed-bg-pattern">

                        <?php $a = 1; foreach( str_replace('/home/valley/web/valley.demo.themexpert.com/public_html/', 'http://valley.demo.themexpert.com/' , glob($this->pattern_path . '/*.jpg'))  as $pattern ):?>
                        	
                        	<li data-bg-img="pattern/<?php echo $a++ ?>.jpg">
                        		<img src="<?php echo $pattern; ?>">
                        	</li>
                        	
                        <?php endforeach;?>

                        </ul>

                    </div>

                </div>
            </li>

            <li class="customizer-tab uk-padding-small" id="customizer-tab">
                <div class="uk-margin uk-container body-bg">
                    <label class="uk-form-label">Body BG Color</label>
                    <div class="uk-form-controls">
                        <input value="616161" class="jscolor uk-input">
                    </div>
                </div>
                <div class="uk-margin uk-container body-text-color">
                    <label class="uk-form-label">Body Text Color</label>
                    <div class="uk-form-controls">
                        <input value="616161" class="jscolor uk-input">
                    </div>
                </div>
                <div class="uk-margin uk-container primary-color">
                    <label class="uk-form-label">Primary Color</label>
                    <div class="uk-form-controls">
                        <input value="00a6ff" class="jscolor uk-input">
                    </div>
                </div>
                <div class="uk-margin uk-container display-none">
                    <label class="uk-form-label">Secondary Color</label>
                    <div class="uk-form-controls">
                        <input value="00a6ff" class="jscolor uk-input">
                    </div>
                </div>
                <div class="uk-margin uk-container link-color">
                    <label class="uk-form-label">Link Color</label>
                    <div class="uk-form-controls">
                        <input value="424242" class="jscolor uk-input">
                    </div>
                </div>
            </li>

            <li class="typo-tab uk-padding-small" id="typo-tab">

                <div class="gf-body">
                    <label class="uk-form-label">Body Font</label>
                    <label class="uk-form-label"></label>
                    <select class="googleFont uk-select" data-use="body"></select>
                </div>

                <div class="body-font-size uk-margin">
                    <label class="uk-form-label">Body Font Size</label>
                    <input type="number" class="font-size uk-input" name="body-font-size" id="body-font-size" value="16" data-font-size="body">
                </div>

                <div class="gf-heading uk-margin">
                    <label class="uk-form-label">Heading Font</label>
                    <label class="uk-form-label"></label>
                    <select class="googleFont uk-select" data-use="heading"></select>
                </div>

            
                <div class="body-font-size uk-margin">
                    <label class="uk-form-label">Heading Font Size</label>
                    <input type="number" class="font-size uk-input" name="body-font-size" id="heading-font-size" value="20" data-font-size="heading">
                </div>
              
            </li>
        </ul>
    </div>
</div>
	<?php
	}
}