/**
 * Created by Anam on 2/24/2017.
 */


var gulp = require('gulp');
var concat = require('gulp-concat');
var less = require('gulp-less');

gulp.task('less', function(){
    return gulp.src('./styles/*.less')
        .pipe(concat('master.css'))
        .pipe(less())
        .pipe(gulp.dest('./dist/styles'))
});

gulp.task('watch', function(){
    console.log('watching');
    gulp.watch('./styles/**/*', ['less']);
});

gulp.task('default', ['watch']);