<?php 
/*
Plugin Name: TX Style Switcher
Plugin URI: http://themexpert.com
Description: A style switcher plugin.
Version: 1.0
Author: ThemeXpert
Author URI: http://themexpert.com
License: GPLv2 or later
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

function style_html(){

?>
<div class="switcher-section">

    <div class="toggle-switcher">
        <a href="javascript:void(0)" class="switcher-spin">
            <i class="fa fa-cog fa-spin"></i>
        </a>
        <a href="#" class="switcher-documentation" target="_blank">
            <i class="fa fa-book"></i>
        </a>
        <a href="#" class="switcher-cart" target="_blank">
            <i class="fa fa-shopping-cart"></i>
        </a>
    </div>

    

    <div class="switcher-section-wrapper">

        <ul class="uk-child-width-expand switcher-tab" uk-tab>
            <li class="uk-active"><a href="#style-tab">Style</a></li>
            <li><a href="#customizer-tab">Customize</a></li>
            <li><a href="#typo-tab">Typography</a></li>
        </ul>

        <ul class="uk-switcher">

            <li class="style-tab uk-padding-small" id="style-tab">

                <div>
                    <label class="uk-form-label">Preset Layouts</label>
                </div>

                <div uk-grid>
                    <div class="uk-width-1-2@m">
                        <div class="uk-card uk-card-default">
                            <a href="/home/?home=true" ><img src="<?php echo plugins_url( 'img/style/thumb1.jpg',  __FILE__); ?>" alt=""></a>
                        </div>
                    </div>

                    <div class="uk-width-1-2@m">
                        <div class="uk-card uk-card-default">
                            <a href="/home-2/?home=true" ><img src="<?php echo plugins_url( 'img/style/thumb2.jpg',  __FILE__); ?>" alt=""></a>
                        </div>
                        
                    </div>
                </div>

                <div class="layout uk-margin">

                    <label class="uk-form-label">Select Layout</label>

                    <select class="uk-select">
                        <option value="full-width">Full Width</option>
                        <option value="boxed">Boxed</option>
                    </select>



                    <div class="boxed-backgrond uk-margin">

                        <label class="uk-form-label">Background Image</label>
                        <ul class="boxed-bg-image">
                            <li data-bg-img="bg/1.jpg"><img src="<?php echo plugins_url( 'img/bg/thumb/1.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="bg/2.jpg"><img src="<?php echo plugins_url( 'img/bg/thumb/2.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="bg/3.jpg"><img src="<?php echo plugins_url( 'img/bg/thumb/3.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="bg/4.jpg"><img src="<?php echo plugins_url( 'img/bg/thumb/4.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="bg/5.jpg"><img src="<?php echo plugins_url( 'img/bg/thumb/5.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="bg/6.jpg"><img src="<?php echo plugins_url( 'img/bg/thumb/6.jpg',  __FILE__); ?>" alt=""></li>
                        </ul>

                        <label class="uk-form-label">Background Pattern</label>
                        <ul class="boxed-bg-pattern">
                            <li data-bg-img="pattern/thumb/1.jpg"><img src="<?php echo plugins_url( 'img/pattern/thumb/1.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="pattern/thumb/2.jpg"><img src="<?php echo plugins_url( 'img/pattern/thumb/2.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="pattern/thumb/3.jpg"><img src="<?php echo plugins_url( 'img/pattern/thumb/3.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="pattern/thumb/4.jpg"><img src="<?php echo plugins_url( 'img/pattern/thumb/4.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="pattern/thumb/5.jpg"><img src="<?php echo plugins_url( 'img/pattern/thumb/5.jpg',  __FILE__); ?>" alt=""></li>
                            <li data-bg-img="pattern/thumb/6.jpg"><img src="<?php echo plugins_url( 'img/pattern/thumb/6.jpg',  __FILE__); ?>" alt=""></li>
                        </ul>

                    </div>

                </div>
                
                
            </li>

            <li class="customizer-tab uk-padding-small" id="customizer-tab">

                <div class="uk-margin uk-container body-bg">
                    <label class="uk-form-label">Body BG Color</label>
                    <div class="uk-form-controls">
                        <input value="616161" class="jscolor uk-input">
                    </div>
                </div>

                <div class="uk-margin uk-container body-text-color">
                    <label class="uk-form-label">Body Text Color</label>
                    <div class="uk-form-controls">
                        <input value="616161" class="jscolor uk-input">
                    </div>
                </div>

                <div class="uk-margin uk-container primary-color">
                    <label class="uk-form-label">Primary Color</label>
                    <div class="uk-form-controls">
                        <input value="00a6ff" class="jscolor uk-input">
                    </div>
                </div>

                <div class="uk-margin uk-container display-none">
                    <label class="uk-form-label">Secondary Color</label>
                    <div class="uk-form-controls">
                        <input value="00a6ff" class="jscolor uk-input">
                    </div>
                </div>

                <div class="uk-margin uk-container link-color">
                    <label class="uk-form-label">Link Color</label>
                    <div class="uk-form-controls">
                        <input value="424242" class="jscolor uk-input">
                    </div>
                </div>

            </li>

            <li class="typo-tab uk-padding-small" id="typo-tab">

                <div class="gf-body">
                    <label class="uk-form-label">Body Font</label>
                    <label class="uk-form-label"></label>
                    <select class="googleFont uk-select" data-use="body"></select>
                </div>

                <div class="body-font-size uk-margin">
                    <label class="uk-form-label">Body Font Size</label>
                    <input type="number" class="font-size uk-input" name="body-font-size" id="body-font-size" value="16" data-font-size="body">
                </div>

                <div class="gf-heading uk-margin">
                    <label class="uk-form-label">Heading Font</label>
                    <label class="uk-form-label"></label>
                    <select class="googleFont uk-select" data-use="heading"></select>
                </div>

            
                <div class="body-font-size uk-margin">
                    <label class="uk-form-label">Heading Font Size</label>
                    <input type="number" class="font-size uk-input" name="body-font-size" id="heading-font-size" value="20" data-font-size="heading">
                </div>
                

            </li>


        </ul>
    </div>
</div>

<?php 

}

add_action ('wp_footer', 'style_html');

function wp_style_switcher_enqueue() {
    //wp_enqueue_style('switcher-bs-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
    //wp_enqueue_style('switcher-fa-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style( 'switcher-uikit-css', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.12/css/uikit.min.css');
        wp_enqueue_style( 'switcher-css', plugins_url( 'dist/styles/master.css', __FILE__ ), array(), null );


    wp_enqueue_style('tx-gf-body','/'); // enqueue for body font family
    wp_enqueue_style('tx-gf-title','/'); // enqueue for title font family
    

    wp_enqueue_script( 'switcher-uikit-js', 'https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.12/js/uikit.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'switcher-color-js', plugins_url( 'js/jscolor.min.js', __FILE__ ), array( 'jquery' ), null, true );
    wp_enqueue_script( 'switcher-main-js', plugins_url( 'js/main.js', __FILE__ ), array( 'jquery' ), null, true );
}

add_action( 'wp_enqueue_scripts', 'wp_style_switcher_enqueue' );

