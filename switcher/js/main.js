/**
 * Created by Anam on 2/24/2017.
 */

jQuery(document).ready(function($) {


    // Toggle the style switcher
    //=============================
    $("a.switcher-spin").on("click", toggleStyleSwitcher);
    function toggleStyleSwitcher(e){
        e.preventDefault();
        var div = $('.switcher-section');

        if (div.css("left") === "-400px") {
            $(".switcher-section").animate({
                left: "0px"
            });
        } else {
            $(".switcher-section").animate({
                left: "-400px"
            });
        }
    }

    // Get google font from Google API
    //====================================
    $.getJSON("https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBme3ryhPMclA04TFNDv1jwbwe0VJYyKnc", function(fonts){
        for (var i = 0; i < fonts.items.length; i++) {
            $('.googleFont')
                .append($("<option></option>")
                    .attr("value", fonts.items[i].family)
                    .text(fonts.items[i].family));
        }
    });


    /**
     * Change width of the site
     ==================================
     */
    $('.boxed-backgrond').addClass('display-none');
    function siteWidth(){
        var boxed = $(this).val();
        if(boxed == 'boxed'){
            $('.boxed-backgrond').removeClass('display-none');
            $('.tx-site-container').addClass('boxed-width');
        }else{
            $('.tx-site-container').removeClass('boxed-width');
            $('.boxed-backgrond').addClass('display-none');
        }
    }
    const siteWidthValue = document.querySelector('.layout select');
    siteWidthValue.addEventListener('change', siteWidth);

    /**
     * Change Background Image
     ======================================
     */
    function changeBackgroundImage(){
        var backgroundParentClass = $(this).closest('ul').attr('class');
        var backgroundUrlPath = $(this).parent().attr('data-bg-img');

        var backgroundUrl = 'http://valley.demo.themexpert.com/wp-content/themes/valley/dist/images/demo/' + backgroundUrlPath;
        if(backgroundParentClass == 'boxed-bg-image'){
            $('body').css({'background-image':'url('+backgroundUrl+')',
                'background-repeat':'no-repeat',
                'background-size': 'cover',
                'background-attachment': 'fixed' });
        }else{
            $('body').css({'background-image':'url('+backgroundUrl+')',
                'background-repeat':'repeat',
                'background-size': 'initial',
                'background-attachment': 'fixed' });
        }
    }
    const backgroundImage = document.querySelectorAll('.boxed-backgrond img');
    backgroundImage.forEach(input => input.addEventListener('click', changeBackgroundImage));


    /* 
    font family change for body and headline
    =============================================
    */
    $('.googleFont').on('change', function(el){
        var gurl = 'http://fonts.googleapis.com/css?family=' + $(this).val().replace(' ', '+'),
            gfont = $(this).val(),
            use = $(this).data('use');
        if (use === 'body'){
            $('body').css('font-family', '"' + gfont + '"');    
            $('#tx-gf-body-css').attr('href', gurl);
        }else if(use === 'heading'){
            $('.tx-heading, h1, h2, h3, h4').css('font-family', '"' + gfont + '"');
            $('#tx-gf-title-css').attr('href', gurl);
        }
    });

    /* 
    font size change for body and headline
    =======================================
    */
    $('.font-size').change(function(el){
        var fontSize = $(this).data('font-size');
        bodySize = $('#body-font-size').val(),
        headingSize = $('#heading-font-size').val();
        if(fontSize === 'body'){
            $('body').css('font-size' , bodySize + 'px');
        }else if(fontSize === 'heading'){
            $('.tx-heading, h1, h2, h3, h4').css('font-size' ,  headingSize + 'px');
        }
    });    

    /**
     Change body Background color
     ===============================
     */
    function bodyBGColor(){
        var bgValue = $(this).val();
        $('.tx-site-container').css('background-color', '#'+bgValue);
    }
    const bodyBgColor = document.querySelector('.body-bg input');
    bodyBgColor.addEventListener('change', bodyBGColor);

    /**
     Change Body text color
     ========================
     */
    function changeBodyColor(){
        var bgColor = $(this).val();
        $('.tx-site-container, .tx-site-container div, .tx-site-container span, .tx-site-container p')
        .css('color', '#'+bgColor);
    }
    const bodyTextColor = document.querySelector('.body-text-color input');
    bodyTextColor.addEventListener('change', changeBodyColor);

    /**
     Change link color
     ======================
     */
    function changeLinkColor(){
        var linkColor = $(this).val();
        $('.tx-site-container a, .tx-site-container .menu-item a, .tx-site-container .widget_nav_menu a').css('color', '#'+linkColor);
    }
    const bodyLinkColor = document.querySelector('.link-color input');
    bodyLinkColor.addEventListener('change', changeLinkColor);

    /**
     change primary color
     =========================
     */
    function changePrimaryColor(){
        var primaryColor = $(this).val();
        $('.tx-site-container .valley-default-btn-2, .tx-site-container .book-now-btn a, .tx-site-container .product-add-to-cart a, .tx-site-container .dropdown-menu, .tx-site-container .owl-carousel-left, .tx-site-container .owl-carousel-right').
        css({'background-color': '#'+primaryColor});

        $('.tx-site-container button').css({'background-color': '#'+primaryColor});
        $('.dest-price, .price span').css({'color': '#'+primaryColor});
    }
    const bodyPrimaryColor = document.querySelector('.primary-color input');
    bodyPrimaryColor.addEventListener('change', changePrimaryColor);


    
});