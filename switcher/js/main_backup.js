/**
 * Created by Anam on 2/24/2017.
 */

jQuery(document).ready(function($) {


    // Toggle the style switcher
    $("a.switcher-spin").on("click", toggleStyleSwitcher);
    function toggleStyleSwitcher(e){
        e.preventDefault();
        var div = $(this).parent();

        if (div.css("left") === "-400px") {
            $(".switcher-section").animate({
                left: "0px"
            });
        } else {
            $(".switcher-section").animate({
                left: "-400px"
            });
        }
    }

    // Get google font from Google API
    $.getJSON("https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBme3ryhPMclA04TFNDv1jwbwe0VJYyKnc", function(fonts){
        for (var i = 0; i < fonts.items.length; i++) {
            $('.googleFont')
                .append($("<option></option>")
                    .attr("value", fonts.items[i].family)
                    .text(fonts.items[i].family));
        }
    });

    //var fontFamilyLink = 'https://fonts.googleapis.com/css?family=';
    function myFont (){
        var fontFamily = 'Roboto';
        var fontFamilyLink = 'https://fonts.googleapis.com/css?family=';
        fontFamilyLink = fontFamilyLink + fontFamily;

        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.id   = 'googleFontFamily';
        link.rel  = 'stylesheet';
        link.title  = 'titlegoes';
        link.type = 'text/css';
        link.href = fontFamilyLink;
        link.media = 'all';
        head.appendChild(link);
    }

    // Primary Font Family
  
    $('.googleFont').on('change', function(el){
        console.log(el);
        var use = $(this).data('use');
        if (use === 'body'){
            console.log('use in body');
        }else if(use === 'heading'){
            console.log('heading');
        }
    });

    function primaryFontFamily(){
        
        if($('link[title="primaryFontFamily"]')){
            $('link[title="primaryFontFamily"]').remove();
        }

        var fontFamily = '';
        fontFamily = $(this).val();
        var ffList = $(this).val() + ',';

        // Here put selector div
        $('.tx-site-container p, .tx-site-container span, .tx-site-container div, .tx-site-container h2, .tx-site-container h3, .tx-site-container h4, .tx-site-container h4 a')
        .css('font-family', ffList + ' sans-serif');

        fontFamily = fontFamily.split(' ').join('+');
        var fontFamilyLink = 'http://fonts.googleapis.com/css?family=';
        fontFamilyLink = fontFamilyLink + fontFamily+'&ver=4.7.3';

        $('#valley-google-fonts-css').attr('href', fontFamilyLink);
        $('#tx-gf-title-css').attr('href', fontFamilyLink);

        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.title  = 'primaryFontFamily';
        link.id  = 'primaryFontFamily-css';
        link.rel  = 'stylesheet';
        link.href = fontFamilyLink;
        link.type = 'text/css';
        link.media = 'all';
        head.appendChild(link);
    }

    /**
     * Change body Background color
     */
    function bodyBGColor(){
        var bgValue = $(this).val();
        $('.tx-site-container').css('background-color', '#'+bgValue);
    }
    /**
     * Change Body text color
     */
    function changeBodyColor(){
        var bgColor = $(this).val();
        $('.tx-site-container, .tx-site-container div, .tx-site-container span, .tx-site-container p')
        .css('color', '#'+bgColor);
    }
    /**
     * Change link color
     */
    function changeLinkColor(){
        var linkColor = $(this).val();
        $('.tx-site-container a, .tx-site-container .menu-item a, .tx-site-container .widget_nav_menu a').css('color', '#'+linkColor);
    }
    /**
     * change primary color
     */
    function changePrimaryColor(){
        var primaryColor = $(this).val();
        $('.tx-site-container .valley-default-btn-2, .tx-site-container .book-now-btn a, .tx-site-container .product-add-to-cart a, .tx-site-container .dropdown-menu, .tx-site-container .owl-carousel-left, .tx-site-container .owl-carousel-right').
        css({'background-color': '#'+primaryColor});

        $('.tx-site-container button').css({'background-color': '#'+primaryColor});
        $('.dest-price, .price span').css({'color': '#'+primaryColor});
    }
    /**
     * change body font size
     */
    function changeBodyFontSize(){
        var bodyFontSize = $(this).val();
        $('.tx-site-container p, .tx-site-container span, .tx-site-container div')
        .css({'font-size': bodyFontSize + 'px'});
    }

    // body select body font
    // const primaryFont = document.querySelector('.primary-font select');
    // primaryFont.addEventListener('change', primaryFontFamily);



    // body background color input
    const bodyBgColor = document.querySelector('.body-bg input');
    bodyBgColor.addEventListener('change', bodyBGColor);

    // body text color input
    const bodyTextColor = document.querySelector('.body-text-color input');
    bodyTextColor.addEventListener('change', changeBodyColor);

    // body link color input
    const bodyLinkColor = document.querySelector('.link-color input');
    bodyLinkColor.addEventListener('change', changeLinkColor);

    // Primary color input
    const bodyPrimaryColor = document.querySelector('.primary-color input');
    bodyPrimaryColor.addEventListener('change', changePrimaryColor);

    // Body font size
    const bodyFontSize = document.querySelector('.body-font-size input');
    bodyFontSize.addEventListener('change', changeBodyFontSize);


    $('.boxed-backgrond').addClass('display-none');

    /**
     * Change width of the site
     */
    function siteWidth(){
        var boxed = $(this).val();
        if(boxed == 'boxed'){
            $('.boxed-backgrond').removeClass('display-none');
            $('.tx-site-container').addClass('boxed-width');
        }else{
            $('.tx-site-container').removeClass('boxed-width');
            $('.boxed-backgrond').addClass('display-none');
        }
    }
    const siteWidthValue = document.querySelector('.layout select');
    siteWidthValue.addEventListener('change', siteWidth);

    /**
     * Change Background Image
     */
    function changeBackgroundImage(){
        var backgroundParentClass = $(this).closest('ul').attr('class');
        var backgroundUrlPath = $(this).parent().attr('data-bg-img');

        var backgroundUrl = 'wp-content/plugins/style-switcher-demo/img/' + backgroundUrlPath;
        if(backgroundParentClass == 'boxed-bg-image'){
            //$('.main-content').css('background', 'white');
            $('body').css({'background-image':'url('+backgroundUrl+')',
                'background-repeat':'no-repeat',
                'background-size': 'cover',
                'background-attachment': 'fixed' });
        }else{
            //$('.main-content').css('background', 'white');
            $('body').css({'background-image':'url('+backgroundUrl+')',
                'background-repeat':'repeat',
                'background-size': 'initial',
                'background-attachment': 'fixed' });
        }
    }
    const backgroundImage = document.querySelectorAll('.boxed-backgrond img');
    backgroundImage.forEach(input => input.addEventListener('click', changeBackgroundImage));
});